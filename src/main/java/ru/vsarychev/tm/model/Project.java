package ru.vsarychev.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import ru.vsarychev.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class Project {

    private String id = UUID.randomUUID().toString();

    private String name;

    private String description;

    private Status status = Status.NOT_STARTED;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateStart = new Date();

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateFinish;

    public Project(String name) {
        this.name = name;
    }

}
